package id.renaldirey.myapplication;

import android.view.View;

/**
 * Created by Kwikku on 31/05/2018.
 */

public class Answer {
    View view;
    float actualPositionX;
    float actualPositionY;
    int position;
    RemoveAnswerView removeAnswerView;
    String text;

    public Answer(View view, float actualPositionX, float actualPositionY) {
        this.view = view;
        this.actualPositionX = actualPositionX;
        this.actualPositionY = actualPositionY;
    }

    public Answer(View view, final float actualPositionX, final float actualPositionY, final int position, String text, final RemoveAnswerView removeAnswerView) {
        this.view = view;
        this.actualPositionX = actualPositionX;
        this.actualPositionY = actualPositionY;
        this.removeAnswerView = removeAnswerView;
        this.position = position;
        this.text = text;

        this.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                removeAnswerView.removeAnswer(position);
                removeAnswerView.removeAnswer(Answer.this);
                v.animate()
                        .x(actualPositionX)
                        .y(actualPositionY);
            }
        });
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public float getActualPositionX() {
        return actualPositionX;
    }

    public void setActualPositionX(float actualPositionX) {
        this.actualPositionX = actualPositionX;
    }

    public float getActualPositionY() {
        return actualPositionY;
    }

    public void setActualPositionY(float actualPositionY) {
        this.actualPositionY = actualPositionY;
    }

    public RemoveAnswerView getRemoveAnswerView() {
        return removeAnswerView;
    }

    public void setRemoveAnswerView(RemoveAnswerView removeAnswerView) {
        this.removeAnswerView = removeAnswerView;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public interface RemoveAnswerView {
        void removeAnswer(Answer answers);
//        void removeAnswer(View view, int positionX);
    }
}
