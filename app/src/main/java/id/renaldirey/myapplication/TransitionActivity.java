package id.renaldirey.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;

public class TransitionActivity extends Activity {

    FlexboxLayout fbLayout;
    FlexboxLayout fbLayout2;
    LinearLayout llLayout;
    int i = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fbLayout = (FlexboxLayout) findViewById(R.id.ly_fb);
        fbLayout2 = (FlexboxLayout) findViewById(R.id.ly_fb2);
        llLayout = (LinearLayout) findViewById(R.id.ly_ll);

        for(int i=0;i<fbLayout2.getChildCount();i++){
            final View views = fbLayout2.getChildAt(i);
            final int finalI = i;
            views.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    addViewFirstFlex(v);
                    addWordToTarget(v);
                    loadTargetLayout();
                }
            });
        }
    }

    public void loadTargetLayout(){
        for(int i=0;i<fbLayout.getChildCount();i++){
            final View views = fbLayout.getChildAt(i);
            final int finalI = i;
            views.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeFromTarget(v);
                }
            });
        }
    }

    public void removeFromTarget(final View view){
        Animation animate = AnimationUtils.loadAnimation(TransitionActivity.this, R.anim.bounce_out);
        animate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //
            }
        });
        view.startAnimation(animate);
    }

    public void addWordToTarget(View view){

//        view.setClickable(false);
        Animation animate = AnimationUtils.loadAnimation(TransitionActivity.this, R.anim.bounce_in);
        Animation shake = AnimationUtils.loadAnimation(TransitionActivity.this, R.anim.shake);
        view.startAnimation(shake);

        int paddingDp = 5;
        float density = getResources().getDisplayMetrics().density;
        int paddingPixel = (int)(paddingDp * density);

        FlexboxLayout.LayoutParams fbParams = new FlexboxLayout.LayoutParams(
                FlexboxLayout.LayoutParams.WRAP_CONTENT,
                FlexboxLayout.LayoutParams.WRAP_CONTENT);
        fbParams.setMargins(paddingPixel,paddingPixel,paddingPixel,paddingPixel);

        TextView tvFrom = (TextView) view;
        TextView tvAdd = new TextView(this);

        tvAdd.setPadding(paddingPixel,paddingPixel,paddingPixel,paddingPixel);
        tvAdd.setLayoutParams(fbParams);
        tvAdd.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.flexbox_item_background));
        tvAdd.setText(tvFrom.getText().toString());

        fbLayout.addView(tvAdd);
        tvAdd.startAnimation(animate);
    }

    public void addViewFirstFlex(final View view){
//        ((ViewGroup) view.getParent()).removeView(view);

        fbLayout2.removeView(view);
        fbLayout.removeView(view);

        fbLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                fbLayout.addView(view);
            }
        }, 750);

//        view.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                fbLayout.addView(view);
//            }
//        }, 750 );

    }

    public void removeViewParentFlex(final View view){
//        ((ViewGroup) view.getParent()).removeView(view);

        fbLayout.removeView(view);
        fbLayout2.removeView(view);

        fbLayout2.postDelayed(new Runnable() {
            @Override
            public void run() {
                fbLayout2.addView(view);
            }
        }, 750);

//        view.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                fbLayout2.addView(view);
//            }
//        }, 1000 );

    }

    public void addFlexbox(View view){
        i++;
        int paddingDp = 5;
        float density = getResources().getDisplayMetrics().density;
        int paddingPixel = (int)(paddingDp * density);

        FlexboxLayout.LayoutParams fbParams = new FlexboxLayout.LayoutParams(
                FlexboxLayout.LayoutParams.WRAP_CONTENT,
                FlexboxLayout.LayoutParams.WRAP_CONTENT);
        fbParams.setMargins(paddingPixel,paddingPixel,paddingPixel,paddingPixel);
        TextView tvAdd = new TextView(this);
        tvAdd.setPadding(paddingPixel,paddingPixel,paddingPixel,paddingPixel);
        tvAdd.setLayoutParams(fbParams);
        tvAdd.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.flexbox_item_background));
        tvAdd.setText("Java "+ i);

        fbLayout.addView(tvAdd);
    }

    public void removeFlexbox(View view){
        fbLayout.removeViewAt(0);
    }
}