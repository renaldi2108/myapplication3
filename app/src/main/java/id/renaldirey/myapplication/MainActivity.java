package id.renaldirey.myapplication;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends Activity implements Answer.RemoveAnswerView {

    FlexboxLayout fbLayout2;
    View viewLneFirst;
    View viewLneSecond;
    View viewLneThird;

    LinkedList<Answer> listAnswers2;

    List<String> listActual;

    float allWitdh = 0F;
    float leftPositions = 0F;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        fbLayout2 = (FlexboxLayout) findViewById(R.id.answerBox);
        viewLneFirst = (View) findViewById(R.id.lineFirst);
        viewLneSecond = (View) findViewById(R.id.lineSecond);
        viewLneThird = (View) findViewById(R.id.lineThird);

        listActual = new ArrayList<>();
        listAnswers2 = new LinkedList<>();
        List<String> listStrings = new ArrayList<>();

        listStrings.add("morning");
        listStrings.add("are");
        listStrings.add("where");
        listStrings.add("not");
        listStrings.add("have");
        listStrings.add("How");
        listStrings.add("How");
        listStrings.add("How");
        listStrings.add("where");
        listStrings.add("not");
        listStrings.add("have");
        listStrings.add("How");

        listActual.add("How");
        listActual.add("are");
        listActual.add("you");

        for (int i = 0; i < listStrings.size(); i++) {
            FlexboxLayout.LayoutParams fbParams = new FlexboxLayout.LayoutParams(
                    FlexboxLayout.LayoutParams.WRAP_CONTENT,
                    FlexboxLayout.LayoutParams.WRAP_CONTENT);
            fbParams.setMargins(20, 30, 20, 30);
            TextView tvAdd = new TextView(this);

            tvAdd.setLayoutParams(fbParams);

            tvAdd.setPadding(40, 20, 40, 20);
            tvAdd.setTextSize(18.0F);
            tvAdd.setBackgroundColor(Color.GREEN);
            tvAdd.setText(listStrings.get(i));

            tvAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    moveToAnswer(v);
                }
            });

            fbLayout2.addView(tvAdd);
        }
    }

    public void moveToAnswer(View view) {

        float topPosition = viewLneFirst.getY() - view.getHeight();
        float leftPosition = 0F;
        listAnswers2.add(new Answer(view, view.getX(), view.getY(), 1, ((TextView) view).getText().toString(), this));

        if (listAnswers2.size() > 1) {
            float allWidth = 0F;
            for (int i = 0; i < listAnswers2.size(); i++) {
                allWidth += (float) listAnswers2.get(i).getView().getWidth() + 20F;
                allWitdh += (float) listAnswers2.get(i).getView().getWidth() + 20F;
            }
            allWidth -= (float) view.getWidth() + 20F;
            allWitdh -= (float) view.getWidth() + 20F;

            View parent = (View)viewLneFirst.getParent();
            int width = parent.getWidth();

            leftPosition = viewLneFirst.getX() + allWidth;

            if((float) viewLneFirst.getWidth() - 240 < leftPosition){
                listAnswers2.getLast().setPosition(2);
                topPosition = viewLneSecond.getY() - view.getHeight();
                leftPosition = 0F;
                allWidth = 0F;

                for (int i = 0; i < listAnswers2.size(); i++) {
                    if(listAnswers2.get(i).getPosition() == 2){
                        allWidth += (float) listAnswers2.get(i).getView().getWidth() + 20F;
                    }
                }

                allWidth -= (float) view.getWidth() + 20F;
                leftPosition = viewLneSecond.getX() + allWidth;

                if((float) viewLneSecond.getWidth() - 240 < leftPosition) {
                    listAnswers2.getLast().setPosition(3);

                    topPosition = viewLneThird.getY() - view.getHeight();
                    leftPosition = 0F;
                    allWidth = 0F;

                    for (int i = 0; i < listAnswers2.size(); i++) {
                        if(listAnswers2.get(i).getPosition() == 3){
                            allWidth += (float) listAnswers2.get(i).getView().getWidth() + 20F;
                        }
                    }

                    allWidth -= (float) view.getWidth() + 20F;
                    leftPosition = viewLneThird.getX() + allWidth;
                }
            }
        }

        view.animate()
                .x(leftPosition)
                .y(topPosition);
    }

    @Override
    public void removeAnswer(Answer answers) {

        allWitdh -= answers.getView().getWidth() + 20F;
        leftPositions = viewLneFirst.getX() + allWitdh;
        listAnswers2.remove(answers);
        float allWidth = 0F;
        float allWidth2 = 0F;
        float allWidth3 = 0F;
        float topPosition = 0F;

        for (int i = 0; i < listAnswers2.size(); i++) {
            final View view = listAnswers2.get(i).getView();

            if(leftPositions < (float) viewLneFirst.getWidth() - 240){
//                Toast.makeText(this, "hayy", Toast.LENGTH_SHORT).show();
            }

            if(listAnswers2.get(i).getPosition() == 1) {
                view.animate().x(allWidth);
                allWidth += (float) view.getWidth() + 20F;
            } else if(listAnswers2.get(i).getPosition() == 2) {
                view.animate().x(allWidth2);
                allWidth2 += (float) view.getWidth() + 20F;
            } else if(listAnswers2.get(i).getPosition() == 3) {
                view.animate().x(allWidth3);
                allWidth3 += (float) view.getWidth() + 20F;
            }
        }

        float width2 = 0F;
        float width3 = 0F;

        for (int j = 0; j < listAnswers2.size(); j++) {
            if(listAnswers2.get(j).getPosition() == 2) {
                final View view2 = listAnswers2.get(j).getView();
                if(viewLneFirst.getX() + allWidth < (float) viewLneFirst.getWidth() - 240) {

                    listAnswers2.get(j).setPosition(1);

                    topPosition = viewLneFirst.getY() - view2.getHeight();

                    view2.animate()
                            .x(allWidth)
                            .y(topPosition);

                    allWidth += (float) view2.getWidth() + 20F;
                    width2 = 0F;
                }else{
                    view2.animate()
                            .x(width2);


                    width2 += (float) view2.getWidth() + 20F;

                }
            }

        }

        for (int k = 0; k < listAnswers2.size(); k++) {
            if(listAnswers2.get(k).getPosition() == 3) {
                final View view3 = listAnswers2.get(k).getView();

                if(viewLneSecond.getX() + width2 < (float) viewLneSecond.getWidth() - 240) {

                    listAnswers2.get(k).setPosition(2);

                    topPosition = viewLneSecond.getY() - view3.getHeight();

                    view3.animate()
                            .x(width2)
                            .y(topPosition);

                    width2 += (float) view3.getWidth() + 20F;
                    width3 = 0F;
                }else{
                    view3.animate()
                            .x(width3);


                    width3 += (float) view3.getWidth() + 20F;
                }
            }

        }



        answers.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToAnswer(v);
            }
        });
    }
}